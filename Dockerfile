# Use Arch Linux latest as a base
FROM archlinux:latest

RUN groupadd -r darkan && useradd -r -g darkan darkan

# Install required packages and tools
RUN pacman -Syu --noconfirm && \
    pacman -S --noconfirm git git-lfs base-devel curl zip unzip

USER darkan

 RUN curl -s "https://get.sdkman.io" | bash && \
    bash -c "source $HOME/.sdkman/bin/sdkman-init.sh && \
    sdk install java 21-tem"

# Clone the repository (replace with your repository URL)
RUN git clone https://gitlab.com/darkanrs/world-server.git /darkan
RUN git clone https://gitlab.com/darkanrs/cache.git /cache

# Add a config file to the repository
COPY worldConfig.json /darkan/
# Note: Make sure you have a 'configfile' in the same directory as the Dockerfile

# Replace placeholder values in the config file with environment variables
# Example: Replace all occurrences of 'CONFIG_VALUE' with the value of $CONFIG_ENV_VAR
RUN sed -i "s/WORLD_NAME/${WORLD_NAME:-Darkan}/g" /darkan/worldConfig.json
RUN sed -i "s/WORLD_OWNER_USERNAME/${WORLD_OWNER_USERNAME:-trent}/g" /darkan/worldConfig.json
RUN sed -i "s/DEBUG/${DEBUG:-false}/g" /darkan/worldConfig.json
RUN sed -i "s/MONGO_URL/${MONGO_URL:-localhost}/g" /darkan/worldConfig.json
RUN sed -i "s/MONGO_PORT/${MONGO_PORT:-27017}/g" /darkan/worldConfig.json
RUN sed -i "s/MONGO_USER/${MONGO_USER:-}/g" /darkan/worldConfig.json
RUN sed -i "s/MONGO_PASSWORD/${MONGO_PASSWORD:-}/g" /darkan/worldConfig.json
RUN sed -i "s/MONGO_DB_NAME/${MONGO_DB_NAME:-darkan-world}/g" /darkan/worldConfig.json
RUN sed -i "s/LOBBY_IP/${LOBBY_IP:-dev.darkan.org}/g" /darkan/worldConfig.json
RUN sed -i "s/LOBBY_API_KEY/${LOBBY_API_KEY:-TEST_API_KEY}/g" /darkan/worldConfig.json
RUN sed -i "s/WORLD_NUMBER/${WORLD_NUMBER:-50}/g" /darkan/worldConfig.json
RUN sed -i "s/WORLD_IP_ADDRESS/${WORLD_IP_ADDRESS:-localhost}/g" /darkan/worldConfig.json
RUN sed -i "s/WORLD_PORT/${WORLD_PORT:-43595}/g" /darkan/worldConfig.json
RUN sed -i "s/WORLD_ACTIVITY/${WORLD_ACTIVITY:-Docker Test World}/g" /darkan/worldConfig.json
RUN sed -i "s/WORLD_COUNTRY_FLAG_ID/${WORLD_COUNTRY_FLAG_ID:-1}/g" /darkan/worldConfig.json
RUN sed -i "s/WORLD_LOGIN_MESSAGE/${WORLD_LOGIN_MESSAGE:-Welcome to Darkan!}/g" /darkan/worldConfig.json

# Make the script executable and run it
WORKDIR /darkan
CMD ["bash", "-c", "source $SDKMAN_DIR/bin/sdkman-init.sh && ./run-release.sh"]

# For the service to always restart, you would handle that in your Docker run command or in a Docker Compose configuration, not in the Dockerfile itself. 
# Example: docker run --restart=always <image_name>
